<?php
try{
	$pdfname = 'test.pdf';
	$rawData = file_get_contents("php://input");
	//Decode pdf content
	$pdf_decoded = base64_decode ($rawData);
	//Write data back to pdf file
	$pdf = fopen ($pdfname,'w');
	fwrite ($pdf,$pdf_decoded);
	//close output file
	$res = 'success';
} catch(Exception $e) {
	$res = 'error';
}

header("content-type: application/json");
echo json_encode(array("res"=>$res));
exit;