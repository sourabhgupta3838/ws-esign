# README #

This is JS project which works as E-Signature(digital) PDF document code developed in JQuery and HTML.

### What is this repository for? ###

* E-Signature(digital) PDF document code
* 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Download or pull the code, put in specific folder and run in any browser
* Configuration - N/A
* Dependencies - JS browser such as Chrome, PHP for Save PDF feature
* Database configuration - N/A
* How to run tests - run in any JS browser
* Deployment instructions - Download the code as it is

### Contribution guidelines ###

* Writing tests - N/A
* Code review - N/A
* Other guidelines - N/A

### Who do I talk to? ###

* Repo owner or admin - Sourabh Gupta
* Other community or team contact