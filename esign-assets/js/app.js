var RAWFILE_PATH = config.rawfile;
var pdf = null;
pdfjsLib.disableWorker = true;
var pages = 0;
// //Prepare some things
// var canvas = document.getElementById('pdf_renderer');
// var context = canvas.getContext('2d');
var scale = 1.4;
var canvasWidth=0;
var canvasHeight=0;
var pageStarts=new Array();
pageStarts[0]=0;
maxCanvasWidth = 900;
var signStatus = 0;
console.log(RAWFILE_PATH);
pdfjsLib.getDocument(RAWFILE_PATH).then((pdf) => {
	//Render all the pages on a single canvas
    pages = pdf.numPages;
    for(var i = 1; i <= pdf.numPages; i ++){
        pdf.getPage(i).then(function getPage(page){
        	var canvas = $("<canvas></canvas>");
        	$(canvas).attr("id", "pdf-page-" + page.pageIndex);
        	$("#canvas_container").append(canvas).ready(function() {
        		renderPDF(page);
        	});
       	});
     }

	if($("#my_pdf_viewer").width() > 1450) {
		var leftpos = Math.round(($("#my_pdf_viewer").width() - $("#canvas_container").width())/30);
		$("#canvas_container").css({left: leftpos + '%'});		
	}
});


function renderPDF(page) {
	var viewport = page.getViewport(scale);
    // changing canvas.width and/or canvas.height auto-clears the canvas
    var canvas = document.getElementById("pdf-page-" + page.pageIndex);
    var context = canvas.getContext('2d');
    canvas.width = viewport.width;
    if(canvas.width>maxCanvasWidth){
       canvas.width = maxCanvasWidth;
    }
    canvas.height = viewport.height;
    page.render({canvasContext: context, viewport: viewport});
}
var mx, my;
$(document).ready(function() {
    var signaturePad = new SignaturePad(
    	document.getElementById('signature-pad'),{
		    minWidth: 0.5,
    		maxWidth: 2.0,
  			penColor: 'rgb(0, 0, 0)',
  			velocityFilterWeight: 0.7,
  			throttle: 16,
  			lineWidth: 1
    	});

    if(localStorage.getItem('sig_img') !== null) {
    	$("#sign_prev").attr("src", localStorage.getItem('sig_img'));
    	$("#sign_prev").on("load", function() {
    		$(".signature.add").hide();
    		$(".signature.imag").show();
    	});
    }

    if(localStorage.getItem("help") == "done") {
    	$(".help-tooltip").hide();
    }

    $(".help-done").click(function () {
    	localStorage.setItem('help', 'done');
    	$(".help-tooltip").hide();
    });

    if(config) {
    	if(config.heading == false) {
    		$(".header").hide();
    	}
    }

    $(".user-image").hover(function () {
    	$(".menu").css("visibility", "visible");
    	setTimeout(function(){
    		$(".menu").css("visibility", "hidden");
    	}, 3000);
	});

    $("#create_sig").click(function() {
    	if($(this).hasClass("signature-disable") === false)
    		$("#esign").show();
    });

    $(".close-esign").click(function () {
    	$("#esign").hide();
    	clearCanvas('signature-pad');
    });

    $("#clear-click").click(function () {
    	clearCanvas('signature-pad');
    });

    $(".btn-cancel").click(function () {
    	window.location.href = config.cancelUrl;
    });

    $('#click').click(function(){
        var data = signaturePad.toDataURL('image/svg+xml');
        $("#sign_prev").attr("src",data);
    });

    $("#upload_sig").click(function() {
    	$("#upload_esign").show();
    });
    
    $(".clear-btn").click(function() {
    	if($(this).hasClass('signature-disable') === false) {
	    	let conf = window.confirm("It will clear the document. Do you want to continue");
	    	if(conf) {
	    		window.location.href = "";
	    	}
	    }
    });

    $("#sign_prev")
	    .on('load', function() {
	    	if($("#esign").css("display") == "block" 
	    		|| $("#upload_esign").css("display") == "block") {
	    		saveSImg($(this));
	    		$(".close-esign").trigger('click');
	    		$(".signature.add").hide();
	    	}
			
			$(this).css("width", "100px");
			$(this).css("height", "70px");
	    	$(".signature.preview").show();
	    	//$(".signature.preview").css("height", "110px");
	    })
	    .on('error', function() { 
	    	console.log("error loading image"); 
	   	});

    window.onscroll = function(ev) {	        
    	pageIndex = pages - 1;
    	if ((window.innerHeight + window.scrollY) >= ($(".pdf-viewer").height() - $("#pdf-page-0").height())) {
	        // you're at the bottom of the page
	        console.log('end reached');
	        $(".right-block").find(".item").css("opacity", "1");
	        $(".right-block").find(".item").removeClass("signature-disable");
	    }
	};

	$("#download-pdf").click(function() {
		pdf = pdfGen();
		pdf.save("download.pdf");	
		window.location.href = '';						
	});

	$(".submit-btn").click(function () {	
		if($(this).hasClass("signature-disable")) {
			return false;
		}
		if(signStatus == 0) {
			error_msg("It seems document is not signed. Please sign the document to proceed.");
			return false;
		}

		var conf = window.confirm("Do you really want to submit document?");
		if(conf) {
			$(".msg-wait").show();
        	$(".footer-loader").show();        	
        	$(this).html('Saving PDF...');
			$(this).attr("disabled","disabled");

			// mimik time for processing
		    setTimeout(function(){
		    	executePDFGen()
		    }, 1000);
		}
	});

	$("#browse-btn").each(function() {
      $(this).click(function() {
        // trigger file upload
        $("#usignimage").trigger('click');
      });
    });

    $('input[type="file"]').change(function(e) {
    	var file = $(this)[0].files[0];
    	if(file.type === 'image/png') {
    		if (file) {
			    const reader = new FileReader();
			    reader.readAsDataURL(file);
			    reader.onload = function(e) {
			        // browser completed reading file - display it
			        let dataURL = e.target.result;
        			$("#sign_prev").attr("src",dataURL);			        
			    }
			}
    	} else {
    		$(this).val('');
    		error_msg('Please select PNG image.');
    		return false;
    	}
    });

    $("#upload-done").click(function() {
		$("#upload_esign").hide();    	
    });

    $(".delete_simg").click(function() {
    	let conf = window.confirm('Do you really want to delete the signature?');
    	if(conf) {
    		localStorage.removeItem('sig_img');
    		$("#sign_prev").attr('src', '');
    		$(".signature.imag").hide();
    		$(".signature.add").show();
    	}
    });
});

function saveSImg(i) {
	var dataURL = $(i).attr('src');
	ajax(config.saveImageURL, dataURL, 'signature.png', 'aftersaveImage');
}

function executePDFGen() {
	pdf = pdfGen();
	var blob = pdf.output();	
	var formData = btoa(blob);
	ajax(config.savePDFURL, formData, config.filename, 'aftersavePDF');
}


function error_msg(msg) {
	$(".msg-error").html(msg);
	$(".msg-error").show();
	setTimeout(function() {
		$(".msg-error").hide();
	}, 2000)
}

function ajax(url, formData, filename, callback) {	
	$.ajax({
		url: url,
        method: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        type: 'json',       
        beforeSend: function(xhrObj){
           xhrObj.setRequestHeader("X-Vendor-ID", config.VendorID);
           xhrObj.setRequestHeader("X-File-Name", filename);
           xhrObj.setRequestHeader("X-Callback", callback);
        },
		success: function(data){
			if(callback == 'aftersaveImage') {
				localStorage.setItem('sig_img', config.userFilesLoc + 'signature.png');
			}
			if(callback == 'aftersavePDF') {
				$(".footer-loader").hide();			
				if(data.res) {
					if(data.res == 'success') {
						$(".msg-success").show();					
						setTimeout(function() { 
							window.location.href = 'test.pdf';
						}, 1200);

						return true;
					}
				}
	        	error_msg("Error in saving file.");        	
	        }
		},
        error: function(data){        	
			if(callback == 'aftersavePDF') {
				$(".footer-loader").hide();
	        	console.log("error",data);
	        	error_msg(data);
	        }
        }
    });
}

function pdfGen() {
	var canvases = $("#canvas_container").find("canvas").length; 
	var options = {
		compress: true,
		unit: 'mm', // "mm", "cm", "m", "in" or "px"
		orientation: 'p',
		format: 'letter'
	};
	var pdf = new jsPDF(options);
	var imgWidth = 210;
	var pageHeight = 295;

	for(i=0; i<canvases;i++) {
		var canvas = document.getElementById('pdf-page-' + i);							
		// only jpeg is supported by jsPDF
		var imgData = canvas.toDataURL("image/png", 1.0);
		
		var imgHeight = canvas.height * imgWidth / canvas.width;
		if(i > 0) {
			pdf.addPage();
		}
		pdf.addImage(imgData, 'PNG', 0, 0, imgWidth, imgHeight, 'pg-'+i, 'FAST');
	}
	return pdf;
}

function clearCanvas(canvas_id) {
	var canvas = document.getElementById(canvas_id);
	const context = canvas.getContext('2d');
	context.clearRect(0, 0, canvas.width, canvas.height);
}

function scrollToBottom(div_id) { 
	//$(document).scrollTop($(document).height());
	//$("#document").animate({ scrollTop: $(document).height()}, 1000);
	$("html, body").animate({
            scrollTop: $(
              'html, body').get(0).scrollHeight
        }, 2000);
}

function dataURLToBlob(dataURL) {
    var BASE64_MARKER = ';base64,';
    if (dataURL.indexOf(BASE64_MARKER) == -1) {
        var parts = dataURL.split(',');
        var contentType = parts[0].split(':')[1];
        var raw = parts[1];
        
        return new Blob([raw], {type: contentType});
    }
    else {
        var parts = dataURL.split(BASE64_MARKER);
        var contentType = parts[0].split(':')[1];
        var raw = window.atob(parts[1]);
        var rawLength = raw.length;
        
        var uInt8Array = new Uint8Array(rawLength);
        
        for (var i = 0; i < rawLength; ++i) {
            uInt8Array[i] = raw.charCodeAt(i);
        }
        
        return new Blob([uInt8Array], {type: contentType});
    }
}

$( document ).on( "mousemove", function( event ) {
  mx= event.pageX;
  my = event.pageY;
});

dragging();
function dragging() {
  var diagram = [];
  var canvas = $('.pdf-viewer');
  $(".tool").draggable({
    helper: "clone",
    cancel: false,
  });

  canvas.droppable({
    drop: function(event, ui) {

      //code for image and divs but problem is 
      //divs are working properly but images are not 
      //showing please try to drag and drop the element   
      	if (ui.helper.hasClass("item")) {
      		var sigHTML = ui.helper.html();

      		console.log("s----", sigHTML);
	        var new_signature;
	        new_signature = $("<div>", {
	            class: "tool_wrapper ui-widget",
	            style: ui.helper.attr("style")
	        });

	        var tool_handle_wrapper = $("<div>", {
	        	class: "tool_handle_wrapper",
	        	id: "tool_handler_wrapper"	
	        });
	        
	        var g= $("<div>", {
	              class: "tool_handle ui-widget-header"
	            })
	            .html("&nbsp;").appendTo(tool_handle_wrapper);

	        var d= $("<div>", {
	              class: "tool_delete ui-widget-header"
	            })
	            .html("&nbsp;").appendTo(tool_handle_wrapper).click(function() {
	            	if (confirm("Are you sure you want to remove this signature?")) {
	              		new_signature.remove();
	            	}
	          });

	        var r= $("<div>", {
	              class: "tool_right ui-widget-header"
	            })
	            .html("&nbsp;").appendTo(tool_handle_wrapper).click(function() {
	            	let canvasid = Math.floor(my / $("#pdf-page-0").height());
	            	let canvas = document.getElementById("pdf-page-" + canvasid);
					let canvasRef = canvas.getBoundingClientRect();
					let ctx = canvas.getContext("2d");


					var imagemarker = document.getElementById("tool_handler_wrapper");
					var markerRef = imagemarker.getBoundingClientRect();

					let img = document.getElementById("sign_prev_cp");
					var imgRef = img.getBoundingClientRect();
					var nWidth = img.naturalWidth;
					var nHeight = img.naturalHeight;
					
					let x = mx - 148;
					let f = 50;
					if(canvasid < 5) {
						f = -8;
					} else if(canvasid > 11) {
						f = 100
					}

					let y = (my - f) - (canvasid * $(canvas).height());
					
					ctx.drawImage(
					  img, x, y, $(img).width(), $(img).height()
					);
					signStatus = 1;
					$(".clear-btn").show();
					new_signature.remove();					
	            });
	        
	        $(tool_handle_wrapper).appendTo(new_signature);
	        
	        var tool = $("<div>", {
	              class: ui.helper.attr("class"),
	              contenteditable: false
	            })
	            .html(sigHTML)
	            .appendTo(new_signature).resizable();
	            
	        
	        tool.css({
	         width: "auto",
	         height: "auto",
	         "line-height": "inherit"
	       });
	     	console.log("s1---", new_signature);
	        //$(tool).find(".signature-icon").remove();
	        tool.removeClass("fake ui-draggable ui-draggable-handle ui-draggable-dragging");
	        new_signature.appendTo($(this));
	        var sigTop = parseInt(new_signature.css("top").slice(0, -2));
	        //new_signature.css("position", "relative");
	        new_signature.css("top", (my - 100) +"px");
	        new_signature.css("left", $("#my_pdf_viewer").width() + $(new_signature).position().left);
	        $(new_signature).find("#sign_prev").css("width", "100%");
	        $(new_signature).find("#sign_prev").css("height", "100%");
	        $(new_signature).find("#sign_prev").attr("id", "sign_prev_cp");
	        new_signature.draggable({
	           handle: ".tool_handle",
	           helper: false,
	            containment: "parent"
	        });
    	}
    }
  });
}